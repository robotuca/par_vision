  /*
   * OpenCV Example using ROS and CPP
   */

  // Include the ROS library
  #include <ros/ros.h>
  // Include opencv2
  #include <opencv2/imgproc/imgproc.hpp>
  #include <opencv2/highgui/highgui.hpp>
  // Include CvBridge, Image Transport, Image msg
  #include <image_transport/image_transport.h>
  #include <cv_bridge/cv_bridge.h>
  #include <sensor_msgs/image_encodings.h>

    
  // Publisher
  ros::Publisher pub_output;
  
  
  // ********************* IMAGE CALLBACK ********************* //
  void image_cb(const sensor_msgs::ImageConstPtr& msg)
  {
    ROS_INFO_STREAM("New Image from " << msg->header.frame_id.c_str());
    
    // Convert ros image to opencv image
    cv_bridge::CvImagePtr cv_ptr_image;
    try
    {
      cv_ptr_image = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    
    // Create opencv image for histogram output
    cv_bridge::CvImagePtr cv_ptr_output(new cv_bridge::CvImage);
    cv_ptr_output->header = msg->header;
    cv_ptr_output->encoding = "bgr8";
    
    // Image processing
    cv::GaussianBlur(cv_ptr_image->image, cv_ptr_output->image, cv::Size(3, 3), 0);    

    
    // Publish outputs
    pub_output.publish(cv_ptr_output->toImageMsg());    
    
  }
  
  
// ********************* MAIN FUNCTION ********************* //
int main(int argc, char **argv) {
	// Initialize the ROS system and become a node.
	ros::init(argc, argv, "image_processing_node");
	ros::NodeHandle nh("~");
	
	std::string image_topic, space;
	nh.param<std::string>("image_topic", image_topic, "/camera/image/image_color_rect");
	nh.param<std::string>("space_name", space, "/image_converter");
	
	static const std::string IMAGE_TOPIC          = image_topic;
	static const std::string PUBLISH_TOPIC_OUTPUT = space+"/image_output";

	pub_output = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_OUTPUT, 5);	
			
	// Create a subscriber object.
	ros::Subscriber sub = nh.subscribe(IMAGE_TOPIC, 1, &image_cb);

	// Let ROS take over.
	ros::spin();
}
