  /*
   * OpenCV Example using ROS and CPP
   */

  // Include the ROS library
  #include <ros/ros.h>
  // Include opencv2
  #include <opencv2/imgproc/imgproc.hpp>
  #include <opencv2/highgui/highgui.hpp>
  // Include CvBridge, Image Transport, Image msg
  #include <image_transport/image_transport.h>
  #include <cv_bridge/cv_bridge.h>
  #include <sensor_msgs/image_encodings.h>

    
  // Publishers
  ros::Publisher pub_hist;
  ros::Publisher pub_kmeans;
  ros::Publisher pub_blur;
  ros::Publisher pub_edges;
  ros::Publisher pub_mask;
  ros::Publisher pub_output;
  
  // ********************* HISTOGRAM ********************* //
  void histogram(cv::Mat *input, cv::Mat *output)
  {
      std::vector<cv::Mat> bgr_planes;
      cv::split( *input, bgr_planes );
      int histSize = 256;
      float range[] = { 0, 256 }; //the upper boundary is exclusive
      const float* histRange[] = { range };
      bool uniform = true, accumulate = false;
      cv::Mat b_hist, g_hist, r_hist;
      cv::calcHist( &bgr_planes[0], 1, 0, cv::Mat(), b_hist, 1, &histSize, histRange, uniform, accumulate );
      cv::calcHist( &bgr_planes[1], 1, 0, cv::Mat(), g_hist, 1, &histSize, histRange, uniform, accumulate );
      cv::calcHist( &bgr_planes[2], 1, 0, cv::Mat(), r_hist, 1, &histSize, histRange, uniform, accumulate );
      int hist_w = 512, hist_h = 400;
      int bin_w = cvRound( (double) hist_w/histSize );
    
      cv::Mat histImage( hist_h, hist_w, CV_8UC3, cv::Scalar( 0,0,0) );
      *output = histImage;
    
      cv::normalize(b_hist, b_hist, 0, (*output).rows, cv::NORM_MINMAX, -1, cv::Mat() );
      cv::normalize(g_hist, g_hist, 0, (*output).rows, cv::NORM_MINMAX, -1, cv::Mat() );
      cv::normalize(r_hist, r_hist, 0, (*output).rows, cv::NORM_MINMAX, -1, cv::Mat() );
      for( int i = 1; i < histSize; i++ )
      {
        cv::line( *output,
              cv::Point( bin_w*(i-1), 
              hist_h - cvRound(b_hist.at<float>(i-1)) ),
              cv::Point( bin_w*(i), 
              hist_h - cvRound(b_hist.at<float>(i)) ),
              cv::Scalar( 255, 0, 0), 2, 8, 0  );
        cv::line( *output, 
              cv::Point( bin_w*(i-1), 
              hist_h - cvRound(g_hist.at<float>(i-1)) ),
              cv::Point( bin_w*(i), 
              hist_h - cvRound(g_hist.at<float>(i)) ),
              cv::Scalar( 0, 255, 0), 2, 8, 0  );
        cv::line( *output, 
              cv::Point( bin_w*(i-1), 
              hist_h - cvRound(r_hist.at<float>(i-1)) ),
              cv::Point( bin_w*(i), 
              hist_h - cvRound(r_hist.at<float>(i)) ),
              cv::Scalar( 0, 0, 255), 2, 8, 0  );
    }
  }
  
  // ********************* K-MEANS ********************* //
  void k_means (cv::Mat *input, cv::Mat *output, int k)
  {
    cv::Mat data;
    (*input).convertTo(data,CV_32F);
    data = data.reshape(1,data.total());

    // do kmeans
    cv::Mat labels, centers;
    cv::kmeans(data, k, labels, cv::TermCriteria(CV_TERMCRIT_ITER, 10, 1.0), 3, 
       cv::KMEANS_PP_CENTERS, centers);
    
    // reshape both to a single row of Vec3f pixels:
    centers = centers.reshape(3,centers.rows);
    data = data.reshape(3,data.rows);

    // replace pixel values with their center value:
    cv::Vec3f *p = data.ptr<cv::Vec3f>();
    for (size_t i=0; i<data.rows; i++)
    {
       int center_id = labels.at<int>(i);
       p[i] = centers.at<cv::Vec3f>(center_id);
    }

    // back to 2d, and uchar:
    *output = data.reshape(3, (*input).rows);
    (*output).convertTo(*output, CV_8U); 
  }
  
  // ********************* CAN DETECTOR ********************* //
  void can_detector(cv::Mat *input, cv::Mat *mask)
  {
    int iLowH  = 159;
    int iHighH = 179;
    int iLowS  = 20; 
    int iHighS = 255;
    int iLowV  = 20;
    int iHighV = 255;
    
    int iLowH2  = 0;
    int iHighH2 = 20;
    int iLowS2  = 20; 
    int iHighS2 = 255;
    int iLowV2  = 20;
    int iHighV2 = 255;
         
    cv::Mat mask1, mask2;
    cv::cvtColor(*input, *mask, cv::COLOR_BGR2HSV);
    cv::inRange(*mask, cv::Scalar(iLowH, iLowS, iLowV), cv::Scalar(iHighH, iHighS, iHighV), mask1);
    cv::inRange(*mask, cv::Scalar(iLowH2, iLowS2, iLowV2), cv::Scalar(iHighH2, iHighS2, iHighV2), mask2);
    
    *mask = mask1 | mask2;

    cv::dilate( *mask, *mask, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(15, 15)) ); 
    cv::erode(*mask, *mask, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(15, 15)) );
    
    // Draw a rectangle in the output image
    cv::Moments oMoments = cv::moments(*mask);
    int posX = oMoments.m10 / oMoments.m00;
    int posY = oMoments.m01 / oMoments.m00;        
    cv::Rect boundRect;
    boundRect = cv::boundingRect( *mask );
    cv::rectangle( *input, boundRect.tl(), boundRect.br(), cv::Scalar(255,0,0), 2 );     
  }
  
  
  // ********************* IMAGE CALLBACK ********************* //
  void image_cb(const sensor_msgs::ImageConstPtr& msg)
  {
    ROS_INFO_STREAM("New Image from " << msg->header.frame_id.c_str());
    
    // Convert ros image to opencv image
    cv_bridge::CvImagePtr cv_ptr_image;
    try
    {
      cv_ptr_image = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    
    // Create opencv image for histogram output
    cv_bridge::CvImagePtr cv_ptr_kmeans(new cv_bridge::CvImage);
    cv_ptr_kmeans->header = msg->header;
    cv_ptr_kmeans->encoding = "bgr8";
    
    // Create opencv image for histogram output
    cv_bridge::CvImagePtr cv_ptr_histogram(new cv_bridge::CvImage);
    cv_ptr_histogram->header = msg->header;
    cv_ptr_histogram->encoding = "bgr8";
    
    // Create opencv image for blurring output
    cv_bridge::CvImagePtr cv_ptr_blur(new cv_bridge::CvImage);
    cv_ptr_blur->header = msg->header;
    cv_ptr_blur->encoding = "bgr8";
    
    // Create opencv image for edges output
    cv_bridge::CvImagePtr cv_ptr_edges(new cv_bridge::CvImage);
    cv_ptr_edges->header = msg->header;
    cv_ptr_edges->encoding = "mono8";
    
    // Create opencv image for mask output
    cv_bridge::CvImagePtr cv_ptr_mask(new cv_bridge::CvImage);
    cv_ptr_mask->header = msg->header;
    cv_ptr_mask->encoding = "mono8";
    
    // k-means
    int k = 5;
    k_means(&cv_ptr_image->image, &cv_ptr_kmeans->image, k);
    
    // Histogram
    histogram(&cv_ptr_image->image, &cv_ptr_histogram->image);

    // Blurring
    cv::GaussianBlur(cv_ptr_image->image, cv_ptr_blur->image, cv::Size(3, 3), 0);    
    //cv::medianBlur(cv_ptr_image->image, cv_ptr_blur->image, 3);    
    
    // Edges
    cv::cvtColor( cv_ptr_image->image, cv_ptr_edges->image, cv::COLOR_BGR2GRAY);
    cv::Canny( cv_ptr_edges->image, cv_ptr_edges->image, 100, 200, 3 );
    
    
    // Creating mask to detect coke can
    can_detector(&cv_ptr_image->image, &cv_ptr_mask->image);
    
          
    // Publish outputs
    pub_hist.publish(cv_ptr_histogram->toImageMsg());    
    pub_kmeans.publish(cv_ptr_kmeans->toImageMsg());    
    pub_blur.publish(cv_ptr_blur->toImageMsg());
    pub_edges.publish(cv_ptr_edges->toImageMsg());
    pub_mask.publish(cv_ptr_mask->toImageMsg());
    pub_output.publish(cv_ptr_image->toImageMsg());    
    
  }
  
  
// ********************* MAIN FUNCTION ********************* //
int main(int argc, char **argv) {
	// Initialize the ROS system and become a node.
	ros::init(argc, argv, "image_processing_node");
	ros::NodeHandle nh("~");
	
	std::string image_topic, space;
	nh.param<std::string>("image_topic", image_topic, "/camera/image/image_color_rect");
	nh.param<std::string>("space_name", space, "/image_converter");
	
	static const std::string IMAGE_TOPIC          = image_topic;
	static const std::string PUBLISH_TOPIC_HIST   = space+"/image_hist";
	static const std::string PUBLISH_TOPIC_KMEANS = space+"/image_kmeans";
	static const std::string PUBLISH_TOPIC_BLUR   = space+"/image_blur";
	static const std::string PUBLISH_TOPIC_EDGES  = space+"/image_edges";
	static const std::string PUBLISH_TOPIC_MASK   = space+"/image_mask";
	static const std::string PUBLISH_TOPIC_OUTPUT = space+"/image_output";

	pub_hist   = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_HIST, 5);
	pub_kmeans = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_KMEANS, 5);
	pub_blur   = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_BLUR, 5);
	pub_edges  = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_EDGES, 5);	
	pub_mask   = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_MASK, 5);	
	pub_output = nh.advertise<sensor_msgs::Image>(PUBLISH_TOPIC_OUTPUT, 5);	
			
	// Create a subscriber object.
	ros::Subscriber sub = nh.subscribe(IMAGE_TOPIC, 1, &image_cb);

	// Let ROS take over.
	ros::spin();
}
