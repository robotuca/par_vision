cmake_minimum_required(VERSION 2.8.3)
project(par_vision)
 
add_definitions(-std=c++11 -Wall -Wextra -Wdeprecated)#-Wpedantic -Werror)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS 
  roscpp 
  
  sensor_msgs
  std_msgs
  cv_bridge
  image_transport
)

find_package(OpenCV REQUIRED)

#add_message_files(
#   DIRECTORY msg
#   FILES
#   ik.msg
#)


#add_service_files(
#   DIRECTORY srv
#   FILES
#   UR5eIK.srv
#)

#generate_messages(DEPENDENCIES roscpp std_msgs geometry_msgs)

catkin_package(
  CATKIN_DEPENDS 
    roscpp
    
    sensor_msgs
    std_msgs
    cv_bridge
    image_transport
)

include_directories(${catkin_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})

add_executable(simple_image_process src/simple_image_process.cpp)
target_link_libraries(simple_image_process ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

add_executable(complete_image_process src/complete_image_process.cpp)
target_link_libraries(complete_image_process ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
